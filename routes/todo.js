const router = require('express').Router()
const TodoController = require('../controller/todoController.js')

// List all todos
router.get('/', TodoController.findAll)

// Detail todo
router.get('/:id', TodoController.findById)

// Create todo
router.post('/', TodoController.create)

// Update todo
router.patch('/:id', TodoController.update)

// Delete todo
router.delete('/:id', TodoController.delete)

module.exports = router
