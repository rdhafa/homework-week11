const express = require('express')
const app = express()
require('dotenv').config()
const routes = require('./routes/')
const { errorHandler } = require('./middlewares/errorHandler.js')

app.use(express.json())

app.use(routes)
app.use(errorHandler)

module.exports = app
