const app = require('../app.js')
const request = require('supertest')
const { sequelize } = require('../models')
const { queryInterface } = sequelize

// beforeAll((done) => {
//   queryInterface
//     .bulkDelete('Todos', null, {})
//     .then((_) => {
//       done()
//     })
//     .catch((err) => {
//       console.log(err)
//       done(err)
//     })
// })

// afterAll((done) => {
//   queryInterface
//     .bulkDelete('Todos', null, {})
//     .then((_) => {
//       done()
//     })
//     .catch((err) => {
//       console.log(err)
//       done(err)
//     })
// })

describe('Testing Todos', () => {
  beforeAll((done) => {
    queryInterface
      .bulkInsert(
        'Todos',
        [
          {
            id: 1,
            title: 'Buat folder homework',
            createdAt: new Date(),
            updatedAt: new Date()
          },
          {
            id: 2,
            title: 'npm init',
            createdAt: new Date(),
            updatedAt: new Date()
          },
          {
            id: 3,
            title: 'Install packages',
            createdAt: new Date(),
            updatedAt: new Date()
          }
        ],
        {}
      )
      .then((_) => {
        done()
      })
      .catch((err) => {
        console.log(err)
        done(err)
      })
  })

  afterAll((done) => {
    queryInterface
      .bulkDelete('Todos', null, {})
      .then((_) => {
        done()
      })
      .catch((err) => {
        console.log(err)
        done(err)
      })
  })

  it('findAll Todos', (done) => {
    request(app)
      .get('/todos')
      .expect('content-type', /json/)
      .expect(200)
      .then((response) => {
        const { body, status } = response

        expect(status).toEqual(200)
        expect(body.length).toEqual(3)

        const todo1 = body[0]

        expect(todo1.title).toBe('Buat folder homework')
        done()
      })
      .catch((err) => {
        console.log(err)
        done(err)
      })
  })

  it('findOne Todo', (done) => {
    request(app)
      .get('/todos/2')
      .expect('content-type', /json/)
      .expect(200)
      .then((response) => {
        const { body, status } = response

        expect(status).toEqual(200)
        expect(body.id).toEqual(2)
        expect(body.title).toBe('npm init')
        done()
      })
      .catch((err) => {
        console.log(err)
        done(err)
      })
  })

  it('create Todo', (done) => {
    request(app)
      .post('/todos')
      .send({
        id: 4,
        title: 'lakukan testing'
      })
      .set('Accept', 'application/json')
      .expect('content-type', /json/)
      .expect(201)
      .then((response) => {
        const { body, status } = response

        expect(status).toEqual(201)
        expect(body.message).toEqual('Todo Created Successfully!')
        done()
      })
      .catch((err) => {
        console.log(err)
        done(err)
      })
  })

  it('update Todo', (done) => {
    request(app)
      .patch('/todos/3')
      .send({
        title: 'instalasi node packages'
      })
      .set('Accept', 'application/json')
      .expect('content-type', /json/)
      .expect(200)
      .then((response) => {
        const { body, status } = response

        expect(status).toEqual(200)
        expect(body.message).toEqual('Todo Updated Successfully!')
        done()
      })
      .catch((err) => {
        console.log(err)
        done(err)
      })
  })

  it('delete Todo', (done) => {
    request(app)
      .delete('/todos/3')
      .expect('content-type', /json/)
      .expect(200)
      .then((response) => {
        const { body, status } = response

        expect(status).toEqual(200)
        expect(body.message).toEqual('Todo Deleted Successfully!')
        done()
      })
      .catch((err) => {
        console.log(err)
        done(err)
      })
  })

  // ERRORS

  it('findOne Todo error', (done) => {
    request(app)
      .get('/todos/666')
      .expect('content-type', /json/)
      .expect(404)
      .then((response) => {
        const { body, status } = response

        expect(status).toEqual(404)
        expect(body.message).toEqual('Todo Not Found!')

        done()
      })
      .catch((err) => {
        console.log(err)
        done(err)
      })
  })

  it('create Todo error', (done) => {
    request(app)
      .post('/todos')
      .send({})
      .set('Accept', 'application/json')
      .expect('content-type', /json/)
      .expect(400)
      .then((response) => {
        const { body, status } = response

        expect(status).toEqual(400)
        expect(body.message).toEqual('Bad Request!')
        done()
      })
      .catch((err) => {
        console.log(err)
        done(err)
      })
  })

  it('update Todo error', (done) => {
    request(app)
      .patch('/todos/3')
      .send({})
      .set('Accept', 'application/json')
      .expect('content-type', /json/)
      .expect(400)
      .then((response) => {
        const { body, status } = response

        expect(status).toEqual(400)
        expect(body.message).toEqual('Bad Request!')
        done()
      })
      .catch((err) => {
        console.log(err)
        done(err)
      })
  })

  it('delete Todo error', (done) => {
    request(app)
      .delete('/todos/3')
      .expect('content-type', /json/)
      .expect(404)
      .then((response) => {
        const { body, status } = response

        expect(status).toEqual(404)
        expect(body.message).toEqual('Todo Already Deleted!')
        done()
      })
      .catch((err) => {
        console.log(err)
        done(err)
      })
  })
})

describe('Testing findAll error', () => {
  beforeEach((done) => {
    queryInterface
      .bulkInsert(
        'Todos',
        [
          {
            id: 1,
            title: 'Buat folder homework',
            isDeleted: true,
            createdAt: new Date(),
            updatedAt: new Date()
          },
          {
            id: 2,
            title: 'npm init',
            isDeleted: true,
            createdAt: new Date(),
            updatedAt: new Date()
          },
          {
            id: 3,
            title: 'Install packages',
            isDeleted: true,
            createdAt: new Date(),
            updatedAt: new Date()
          }
        ],
        {}
      )
      .then((_) => {
        done()
      })
      .catch((err) => {
        console.log(err)
        done(err)
      })
  })

  afterEach((done) => {
    queryInterface
      .bulkDelete('Todos', null, {})
      .then((_) => {
        done()
      })
      .catch((err) => {
        console.log(err)
        done(err)
      })
  })

  it('findAll Todos error', (done) => {
    request(app)
      .get('/todos')
      .expect('content-type', /json/)
      .expect(404)
      .then((response) => {
        const { body, status } = response

        console.log(body)

        expect(status).toEqual(404)
        expect(body.message).toEqual('Todo Not Found!')
        done()
      })
      .catch((err) => {
        console.log(err)
        done()
      })
  })
})

// Kurang testing kalau internal server error, ./models/index.js,
