'use strict'

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.bulkInsert(
      'Todos',
      [
        {
          title: 'Buat folder homework',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          title: 'npm init',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          title: 'Install packages',
          createdAt: new Date(),
          updatedAt: new Date()
        }
      ],
      {}
    )
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Todos', null, {})
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
}
