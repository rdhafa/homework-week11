const { Todo } = require('../models')
const { TodoError } = require('../middlewares/errorHandler.js')

class TodoController {
  static async findAll(req, res, next) {
    try {
      const result = await Todo.findAll({ where: { isDeleted: false } })
      if (!result[0]) throw new TodoError('TodoNotFound', 'Todo Not Found')

      res.status(200).send(result)
    } catch (err) {
      next(err)
    }
  }

  static async findById(req, res, next) {
    try {
      const { id } = req.params
      const result = await Todo.findByPk(id)
      if (!result) throw new TodoError('TodoNotFound', 'Todo Not Found')

      res.status(200).send(result)
    } catch (err) {
      next(err)
    }
  }

  static async create(req, res, next) {
    try {
      const { title } = req.body
      if (!title) throw new TodoError('BadRequest', 'Bad request')

      const create = await Todo.create({ title })
      if (!create) throw new TodoError('ServerError', 'Internal server error')

      res.status(201).send({ message: 'Todo Created Successfully!' })
    } catch (err) {
      next(err)
    }
  }

  static async update(req, res, next) {
    try {
      const { id } = req.params
      const result = await Todo.findByPk(id)
      if (!result) throw new TodoError('TodoNotFound', 'Todo Not Found')

      const { title, isDeleted } = req.body
      if (title === undefined && isDeleted === undefined) {
        throw new TodoError('BadRequest', 'Bad Request')
      }
      const newData = { title, isDeleted }
      const update = await Todo.update(newData, { where: { id } })
      if (!update) throw new TodoError('ServerError', 'Internal server error')

      res.status(200).send({ message: 'Todo Updated Successfully!' })
    } catch (err) {
      next(err)
    }
  }

  static async delete(req, res, next) {
    try {
      const { id } = req.params
      const findTodo = await Todo.findByPk(id)
      if (!findTodo) throw new TodoError('TodoNotFound', 'Todo Not Found')
      if (findTodo.isDeleted === true)
        throw new TodoError('AlreadyDeleted', 'Todo already deleted')

      const update = await Todo.update({ isDeleted: true }, { where: { id } })
      if (!update) throw new TodoError('ServerError', 'Internal server error')

      res.status(200).send({ message: 'Todo Deleted Successfully!' })
    } catch (err) {
      next(err)
    }
  }
}

module.exports = TodoController
