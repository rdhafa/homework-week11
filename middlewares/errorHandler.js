class TodoError extends Error {
  constructor(errorName, message) {
    super(message)
    this.name = errorName
  }
}

const errorHandler = (err, req, res, next) => {
  if (err.name === 'TodoNotFound') {
    res.status(404).send({ message: 'Todo Not Found!' })
  } else if (err.name === 'BadRequest') {
    res.status(400).send({ message: 'Bad Request!' })
  } else if (err.name === 'AlreadyDeleted') {
    res.status(404).send({ message: 'Todo Already Deleted!' })
  } else {
    res.status(500).send({ message: 'Internal Server Error!' })
  }
}

module.exports = { errorHandler, TodoError }
